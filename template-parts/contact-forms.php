<div class="reveal" id="call-back" data-reveal>
  <div class="h2 text-center margin-medium-b">Перезвоните мне</div>
  <div class="modal-form">
    <?php echo do_shortcode('[contact-form-7 id="132" title="Перезвоните мне"]'); ?>
  </div>
  <button class="close-modal" data-close aria-label="Close modal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
</div>